﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class NewBehaviourScript : MonoBehaviour
{
    // Beviteli mezők definiálása(Tantárgyak)
    public InputField targy1;
    public InputField targy2;
    public InputField targy3;
    public InputField targy4;
    public InputField targy5;
    public InputField targy6;
    public InputField targy7;
    public InputField targy8;
    public InputField targy9;
    public InputField targy10;
    public InputField targy11;
    public InputField targy12;
    // Kiiratáshoz szükséges Text mező definiálása
    public Text ftext;
    // Beviteli mezők definiálása(Kreditek)
    public InputField kredit1;
    public InputField kredit2;
    public InputField kredit3;
    public InputField kredit4;
    public InputField kredit5;
    public InputField kredit6;
    public InputField kredit7;
    public InputField kredit8;
    public InputField kredit9;
    public InputField kredit10;
    public InputField kredit11;
    public InputField kredit12;
    // Használt változók
    public int i = 0, num = 0, teljkredit = 0, felvkredit = 0;
    public double kki = 0.0, ki = 0.0;

    // Használt Listák 
    public List<int> intList = new List<int>();
    public List<int> intList2 = new List<int>();

    public void SetGet()
    {
        // Az összes jegz változót betesszük egy listába, igy a továbbiakban könnyebb végigmenni az adatokon, itt alakitjuk át int-é
        intList.Add(int.Parse(targy1.text));
        intList.Add(int.Parse(targy2.text));
        intList.Add(int.Parse(targy3.text));
        intList.Add(int.Parse(targy4.text));
        intList.Add(int.Parse(targy5.text));
        intList.Add(int.Parse(targy6.text));
        intList.Add(int.Parse(targy7.text));
        intList.Add(int.Parse(targy8.text));
        intList.Add(int.Parse(targy9.text));
        intList.Add(int.Parse(targy10.text));
        intList.Add(int.Parse(targy11.text));
        intList.Add(int.Parse(targy12.text));

        // Szintén átteszük listába, és átalakitjuk int-é
        intList2.Add(int.Parse(kredit1.text));
        intList2.Add(int.Parse(kredit2.text));
        intList2.Add(int.Parse(kredit3.text));
        intList2.Add(int.Parse(kredit4.text));
        intList2.Add(int.Parse(kredit5.text));
        intList2.Add(int.Parse(kredit6.text));
        intList2.Add(int.Parse(kredit7.text));
        intList2.Add(int.Parse(kredit8.text));
        intList2.Add(int.Parse(kredit9.text));
        intList2.Add(int.Parse(kredit10.text));
        intList2.Add(int.Parse(kredit11.text));
        intList2.Add(int.Parse(kredit12.text));

        // Ciklussal végigmegyünk a listák összes elemén
        while (i <= 11)
        {
            // Amennyiben a jegy 1-es megszorozzuk 0-val a krediteket meg hozzáadjuk a felvett viszont nem teljesitett kredithez
            if (intList[i] == 1)
            {
                num = num + (intList[i] * 0);
                felvkredit = felvkredit + intList2[i];
                i++;
            }
            // Amennyiben a jegy 1 -esnél jobb abban az esetben megszorozzuk a hozzátartozó kreddittel a krediteket ezzuttal a teljesitett kreditekhez adjuk
            else if (intList[i] > 1)
            {
                num = num + (intList[i] * intList2[i]);
                teljkredit = teljkredit + intList2[i];
                i++;

            }
            // Amennyiben a jegy 0 abban az esetben csak a ciklus változóját növeljük
            else if (intList[i] == 0)
            {
                i++;
            }

         
         

        }
        // Itt összeadjuk a felvett kreditett
        felvkredit = felvkredit + teljkredit;
        // ki számolunk
        ki = num / 30.0;
        //kki számolunk
        kki = (ki * teljkredit) / felvkredit;
        // Mindent átalakitunk stringgé, hogy az ftexttel kiirassuk
        ftext.text = "KI: "+ ki.ToString("0.00")+"\n" + "KKI: " +kki.ToString("0.00")+"\n" +"Felvett kredit: " + felvkredit.ToString() +"\n"+ "Teljesitett: " + teljkredit.ToString();
       
       
        
    }





}


